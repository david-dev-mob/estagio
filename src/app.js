'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const router = express.Router();


app.use(bodyParser.urlencoded ({extended: true}));

//Definindo view
app.set("view engine","ejs");

//Carregar rotas
const indexRoute = require('./routes/index-route');

//Publicando Rotas
app.use('/', indexRoute);

module.exports = app;