'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/index-controllers');

router.get("/", controller.get);

router.post("/addtask", controller.post);

router.post("/removetask", controller.postR);

module.exports = router;