'use strict';

const view = '../src/views/index.ejs';
const router = require('../routes/index-route');

var task = ["Dar banho no cachorro"];

var complete = [];

exports.get = (req,res,next) => {
    try{
         res.status(200).send(
            res.render(view, {task: task, complete: complete})
        );
    }catch(e){
        res.status(500).send({
            message:'Falha ao buscar tarefa'
        })
    }
}

exports.post = async(req,res,next) => {
    try{
        var newtask = await req.body.newtask;
        task.push(newtask);
        res.redirect('/');
    }catch(e){
        res.status(500).send({
            message:'Falha ao cadastrar tarefa'
        })
    }
}

exports.postR = async(req, res, next) => {
        var completeTask = await req.body.check;
        if(typeof completeTask === "string"){

            complete.push(completeTask);

            task.splice(task.indexOf(completeTask),1);

        }else if(typeof completeTask === "object"){

           for(var i = 0; i < completeTask.lenght; i++){

            complete.push(completeTask[i]);

            task.splice(task.indexOf(completeTask[i]),1);
           } 
        }
        res.redirect('/');
}